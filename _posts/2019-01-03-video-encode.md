---
layout: post
title: "理解视频"
description: "视频发展历程及常用视频编码格式"
categories: [清单]
tags: [基础知识]
redirect_from:
  - /2019/01/03/
---

* Kramdown table of contents
{:toc .toc}
# 理解视频

## 一、 视频的发展演变

&emsp;&emsp;1839年路易·达盖尔的银版摄影法宣告成功，约翰·赫歇尔则第一次创造出**photograph**名词，其意为“drawing with light”，在诸多发明者的共同努力下，摄影这一记录手段在世界上逐渐传播开来，摄影所用的银版也发展为更加优良轻便的胶片。

<center>
<img src="https://i.loli.net/2019/01/03/5c2d6c5896707.gif" alt="Muybridge_race_horse_animated.gif" title="Muybridge_race_horse_animated.gif" />
</center>
<center  style="font-size:90%">
图1.1 Sallie Gardner at a Gallop（1878年由埃德沃德·迈布里奇所摄）
</center>

&emsp;&emsp;物体在快速运动时，当人眼所看到的影像消失后，人眼仍能继续保留约0.1-0.4秒左右的影像，这便是视觉暂留现象，这个现象使得记录运动影像成为可能。随着摄影技术发展日趋成熟，越来越多的人对拍摄运动物体产生兴趣，并尝试使用不同的方法去实践。伴随着胶片技术的发展，1878年埃德沃德·迈布里奇创作了第一个实时的运动图像序列。随后，专门用于拍摄运动图像序列的摄影机问世，法国卢米埃兄弟于1895年第一次公开放映了运动图像序列，从此，**motion picture**，电影这一媒介在世界上开始不断发展与传播。由于电影的制作与放映依赖主要以胶片作为载体，因此**film**一词也成为电影的代名词。

<center>
<img src="https://i.loli.net/2019/01/04/5c2ebc9cda89a.png" alt="Early_Television_System_Diagram.png" title="Early_Television_System_Diagram.png" />
</center>
<center  style="font-size:90%">
图1.2 电视系统的示意图（1928年C.G.B.Rowe发布于Radio News, "Television Comes to the Home"）
</center>

&emsp;&emsp;电影虽然能够记录运动的影像，但是限制于其制作、存储以及放映的条件，并未走进人们的日常生活，而是成为在影院里才能观看的类似于戏剧的艺术产物。但是运动的影像具有更吸引人的表现能力得到了广泛印证，播放运动影像的设备从而受到了广泛的关注与研究，以欺将运动的影像带进千家万户。与此同时，伴随着无线电通信系统的发展，在1909年便首次实现了静止图像的实时传输。随后，查尔斯·弗朗西斯·詹金斯在1913年公开发表了一篇名为“Motion Pictures by Wireless”的文章，指导了运动图像传输系统的设计。1925年机械电视系统首次得到了公开展示，1928年电子电视系统也得到了公开展示。随后，电视系统的技术手段得到进一步发展与提升，将运动影像带入到了人们的家庭日常生活，也促使视频这一媒介得到广泛发展。

&emsp;&emsp;电影电视的发展，不仅仅给人们带来了生活、艺术上的娱乐，更将视频这一丰富的表现手段推进到了社会生活的方方面面。随着数字技术的发展，视频也从模拟信号向数字信号发展。1986年，第一个数字录制视频标准4:2:2 D-1面世。随后，与数字视频相关的格式伴随着视频录制、播放设备的更新而不断迭代，其在传输、存储等不同层次有着各自的标准。在如今的数字社会中，影响最大的是视频编码格式。忽略视频内在的编码格式，表现在外的格式则主要包括分辨率、帧率等。常见的视频分辨率帧率格式如表1.1所示。

<center  style="font-size:90%">
表1.1 常见视频分辨率格式
</center>

| 格式 | 分辨率和帧率 | 宽高比 |
| :--: | :----------: | :----: |
|   4K UHD   |  3840×2160; 23.976p, 24p, 50p, 59.94p, 60p  |   16:9     |
| HD | 1920×1080; 23.976p, 24p, 50i,  59.94i, 25p, 50p, 59.94p, 60p | 16:9 |
| SD | 720×480, 59.94i; 720×576 50i | 4:3; 16:9 |

> 受限于电路设计，早期电视场刷新频率与电源频率相同，并逐渐被固定为电视标准，而电源频率全球并不相同（50Hz/60Hz），因此产生了两种不同的电视制式PAL（50i/25p）和NTSC（60i/30p），录制视频信号转换为电视信号时会产生23.976（24/1.001）和29.97（30/1.001）这样奇怪的帧率。

## 二、 视频编码格式

### 2.1 MPEG与VCEG

&emsp;&emsp;Moving Picture Experts Group（MPEG）是一个权威机构的工作组，其由国际标准组织（[International Organization for Standardization](https://en.wikipedia.org/wiki/International_Organization_for_Standardization), ISO）和国际电工委员会
（[International Electrotechnical Commission](https://en.wikipedia.org/wiki/International_Electrotechnical_Commission), IEC）组成，官方名称为ISO/IEC Joint Technical Committee 1, Subcommittee 29, Working Group 11（ISO/IEC JTC 1/SC 29/WG 11），负责为音视频的压缩传输设定标准，成立于1988年。

&emsp;&emsp;Video Coding Experts Group（VCEG）是国际电信联盟电信标准化部门（[International Telecommunication Union Telecommunication Standardization Sector](https://en.wikipedia.org/wiki/ITU-T), ITU-T）的一个工作组，官方名称为ITU-T SG16/Q.6（Study Group 16 / Question 6）。其负责视频编码标准，包括“H.26x”视频编码标准、“T.8xx”系列图像编码标准和相关技术。

&emsp;&emsp;MPEG标准由不同的部分组成，每个部分都涵盖整个规范的某个方面，其主要包括以下压缩格式和辅助标准：

1. MPEG-1 (1993)：用于运动图像和相关音频在数字存储介质中的编码，最高约为1.5 Mbit/s（ISO/IEC 11172）。这个初始版本是有损文件格式，是第一个用于音频和视频的MPEG压缩标准。其将运动图像和声音的编码比特率设计为能够满足写入光盘的需求，对图像进行下采样，限制帧率在24-30Hz之间，包括流行的MPEG-1 Audio Layer III（MP3）音频压缩格式。其被用作Video CD（VCD）的压缩方案。
2. MPEG-2 (1995)：运动图像和相关音频信息的通用编码（ISO/IEC 13818），用于广播电视传输、视频和音频的标准。其被用作数字电视、数字卫星电视、数字有线电视等电视服务标准，此外，还包括SVCD和DVD视频光盘。
3. MPEG-3：与MPEG-2合并。
4. MPEG-4 (1998):视听对象的编码（ISO/IEC 14496），MPEG-4实现了比MPEG-2更高的压缩因子，此外，MPEG-4更接近计算机图形应用。在更复杂的配置文件中，MPEG-4解码器有效地成为渲染处理器，压缩比特流描述三维形状和表面纹理。MPEG-4支持知识产权管理和保护（IPMP），它提供了使用专有技术来管理和保护比如数字版权等内容的工具。其还支持MPEG-J，这是一个完全编程的解决方案，用于创建自定义交互式多媒体应用程序（带有Java API的Java应用程序环境）和许多其他功能。MPEG-4 AVC用于HD DVD和蓝光光盘。

&emsp;&emsp;上述四个部分的标准是随着时间而发展演变的，主导的视频压缩标准发展历程如表2.1所示。由MPEG和VCEG主导的视频格式标准对于商业应用需要缴纳版税获得商业许可证授权，对于非商业用途提供了GNU（[GNU's Not Unix!](https://en.wikipedia.org/wiki/GNU)）通用公共许可证。非盈利组织[VideoLan](https://en.wikipedia.org/wiki/VideoLAN)开发了用于将视频编码为H.264/MPEG-4 AVC、H.265/MPEG-H HEVC的免费开源软件库x264、x265。

&emsp;&emsp;随着互联网的发展，传统的视频标准主导者MPEG也受到了主要由互联网企业组成和资助的开放媒体联盟（[Alliance for Open Media](https://en.wikipedia.org/wiki/Alliance_for_Open_Media)）的挑战。2015年9月1日，开放媒体联盟宣称其目标是开发免版税视频格式，作为H.264和HEVC等许可格式的替代方案，2018年其公布了用于互联网传输的视频标准[AV1](https://en.wikipedia.org/wiki/AV1)。

<center  style="font-size:90%">
表2.1 视频压缩标准发展时间表
</center>

| 时间 | 标准 | 发布组织 | 应用 |
| :--: | :---: | :----: | :--: |
| 1988 | H.261 | ITU-T | 视频会议, 可视电话 |
| 1993 | MPEG-1 Part 2 | ISO, IEC | VCD |
| 1995 | H.262/MPEG-2 Part 2 | ISO, IEC, ITU-T | SVCD, DVD, 蓝光光盘, 数字电视  |
| 1996 | H.263 | ITU-T | 视频会议, 可视电话, 手机视频（3GP） |
| 1999 | MPEG-4 Part 2 | ISO, IEC | 互联网视频 |
| 2003 | H.264/MPEG-4 AVC | ISO, IEC | 蓝光光盘, HD DVD, 视频会议, 互联网视频 |
| 2013 | H.265/MPEG-H HEVC | ISO, IEC | 超高清蓝光, 超高清流媒体 |

&emsp;&emsp;Joint Video Team（JVT）是VECG和MPEG之间的联合项目，用于发展新的视频编码推荐方式和国际标准。成立于2001年，主要成果为[H.264/MPEG-4 AVC](https://en.wikipedia.org/wiki/H.264/MPEG-4_AVC)。

&emsp;&emsp;Joint Collaborative Team on Video Coding （JCT-VC）MPEG同样也是由VCEG和MPEG专家组成，成立于2010年，负责开发[High Efficiency Video Coding](https://en.wikipedia.org/wiki/High_Efficiency_Video_Coding)（HEVC），其与H.264相比，进一步降低了高质量视频编码所需要的数据比特率。

### 2.2 Advanced Video Coding

&emsp;&emsp;H.264/MPEG-4 Part 10，高级视频编码（MPEG-4 AVC）是一种基于块的运动补偿视频压缩标准。截至2014年，它是录制、压缩和分发视频内容最常用的格式之一，支持高达8192×4320的分辨率，包括8K UHD。

<center>
<img src="https://i.loli.net/2019/01/04/5c2f18611cd4b.jpg" alt="H.264_block_diagram_with_quality_score.jpg" title="H.264_block_diagram_with_quality_score.jpg" />
</center>
<center  style="font-size:90%">
图2.1 H.264编码算法框图
</center>

&emsp;&emsp;其主要特点包括：
1. 多画面画面间预测：更灵活地使用参考帧；可变块大小运动补偿；运动补偿达到四分之一像素精度。
2. 用于“帧内”编码的相邻块的边缘的空间预测
3. 无损宏块编码
4. 灵活的隔行扫描视频编码

### 2.3 High Efficiency Video Coding

&emsp;&emsp;HEVC也称为H.265/MPEG-H Part2，是广泛使用的AVC（H.264/MPEG-4 Part 10）的后继者，下一代视频压缩标准，支持高达8192×4320的分辨率，包括8K UHD。

&emsp;&emsp;与H.264/MPEG-4 AVC HP相比，HEVC被设计为显着提高编码效率，即以可比较的图像质量将比特率要求降低一半，但代价是计算复杂性增加，HEVC的设计目标是允许视频内容的数据压缩率高达1000：1。与H.264/MPEG-4 AVC相比，改进HEVC的两个关键特性是支持更高分辨率的视频和改进的并行处理方法。

<center>
<img src="https://i.loli.net/2019/01/04/5c2f186634dd4.png" alt="HEVC_Block_Diagram.png" title="HEVC_Block_Diagram.png" />
</center>
<center  style="font-size:90%">
图2.2 H.265编码算法框图
</center>

### 2.4 视频编码相关细节

#### 2.4.1 视频压缩帧类型

> 术语“帧”（frame）和“图片”（picture）通常可互换使用，但术语图片是更一般的概念，可以是帧或场（field）。帧是完整图像，而场是组成部分图像的奇数或偶数扫描线的集合。

&emsp;&emsp;视频压缩通常会对不同的视频帧使用不同的压缩算法，其各自有自己的优缺点，主要在压缩率上有所不同。视频帧使用的不同算法通常使用帧类型或者图片类型来进行表示。目前，主要使用的帧类型有三种：**I**, **P**, **B**。

<center>
<img src="https://i.loli.net/2019/01/08/5c3441f32144e.png" alt="I_P_and_B_frames.png" title="I_P_and_B_frames.png" />
</center>
<center  style="font-size:90%">
图2.3 视频帧序列示意图
</center>

- **I**帧意指帧内编码帧（Intra-coded picture），又称关键帧（keyframes），其压缩性最小，不需要其他视频帧来进行解码，是一幅完整的图像，如JPG、BMP等格式。
- **P**帧意指预测帧（Predicted picture），又称变化帧（delta‑frames），压缩性比**I**帧更好，仅保留先前帧中图像内容变化的部分，不需要保存图像背景不变的部分，使用先前帧（previous frames）来解压缩。
- **B**帧意指双向预测帧（Bidirectional predicted picture），压缩性最大，同时使用先前帧和之后帧进行数据参考来解压缩。

&emsp;&emsp;在H.264/MPEG-4 AVC标准中，预测类型的粒度被降低到“切片级别”。切片（Slices）是帧空间上不同的区域，其与同一帧中的其他区域分开编码。

#### 2.4.2 宏块

&emsp;&emsp;宏块（Macroblocks）是基于线性块变换（例如离散余弦变换）的图像和视频压缩格式的处理单元。宏块通常由16×16个采样点组成，并且可进一步细分为变化块和预测块。其用作JPEG, H.261, MPEG-1 Part 2, H.262/MPEG-2 Part 2, H.263, MPEG-4 Part 2, 以及 H.264/MPEG-4 AVC等格式标准的基本处理单元，而在H.265/HEVC中，作为基本处理单元的宏块已被编码树单元替换。

&emsp;&emsp;通常，帧被分段为宏块（Macroblocks），可以在宏块的基础上选择各个预测类型，对于整个帧宏块类型并非是完全相同的。如下所示：

- **I**帧只能包含帧内宏块。
- **P**帧可以包含帧内宏块或预测宏块。
- **B**帧可以包含帧内宏块、预测宏块或双向预测宏块。

#### 2.4.3 运动补偿

&emsp;&emsp;运动补偿是指在给定前一帧和/或未来帧的情况下通过相机和/或视频中对象的运动来预测视频帧。其被用于视频数据编码中的视频压缩，根据参考图像到当前图像的变换来描述图像，当可以从参考图像精确地合成当前图像时，可以提高压缩效率。

&emsp;&emsp;运动补偿建立在这样的事实之上：通常，对于电影的许多帧，一帧与另一帧之间的唯一差异是相机移动或帧中的对象移动的结果，这意味着表示一帧的大部分信息将与下一帧中使用的信息相同。使用运动补偿，视频流通常包含一些完整（参考）帧，而其间的帧所存储的唯一信息则是将前一帧变换到下一帧所需的信息（运动向量）。

<center>
<img src="https://i.loli.net/2019/01/08/5c344dd553e77.png" alt="Elephantsdream_vectorstill04_crop.png" title="Elephantsdream_vectorstill04_crop.png" />
</center>
<center  style="font-size:90%">
图2.4 运动补偿示意图
</center>

## 三、 视频文件与封装格式

&emsp;&emsp;视频文件通常由容器构成，其通常包含使用视频编码格式的视频数据以及使用音频编码格式的音频数据。容器还可以包含同步信息，字幕和元数据（如标题）。用于解码压缩视频或音频的程序（或硬件）称为编解码器；播放或编码视频文件有时需要用户安装与文件中使用的视频和音频编码类型相对应的编解码器库。标准化（或在某些情况下是事实上的标准）视频文件类型是由对容器格式以及允许的视频和音频压缩格式进行限制指定的配置文件，同时具有相应的文件扩展名，使得用户更容易选择对应的编解码器。常见的视频文件及其封装格式如表3.1所示。

<center  style="font-size:90%">
表3.1 视频频文件与封装格式
</center>

| 文件名 | 扩展名 | 容器格式 | 视频编码格式 | 音频编码格式 |
| :---: | :---: | :----: | :------: | :------: |
| Matroska | .mkv | Matroska多媒体容器 | 任意 | 任意 |
| MPEG Transport Stream | .ts, .mts, .m2ts | AVCHD | AVCHD(MPEG-4/H.264) | Dolby AC-3, PCM |
| QuickTime File Format | .mov, .qt | QuickTime | 大部分格式 | AAC, MP3等 |
| MPEG-4 Part 14(MP4) | .mp4, .m4p, .m4v | MPEG-4 Part 12 | H.264, MPEG-4 Part 2, MPEG-2, MPEG-1 | AAC, MP3等 |
| Raw video format	 | .yuv | - | - | - |

### 3.1 yuv文件格式

&emsp;&emsp;YUV是一种色彩编码系统，它从人类对彩色图像或视频的感知角度进行编码，减少了色度分量的带宽，使其在人类感知角度能够比使用RGB更有效地掩盖传输错误或压缩伪像。从发展历史来讲，YUV最初是用于电视系统中黑白到彩色的转换，其针对的是模拟信号；YCbCr色彩空间由RGB色彩空间数学坐标进行变换而定义，针对的是数字信号。如今，YUV一词在计算机领域得到了广泛使用，主要用于描述使用YCbCr编码的文件格式。

&emsp;&emsp;国际电信联盟无线电通信部门（[ITU Radiocommunication Sector](https://en.wikipedia.org/wiki/ITU-R), ITU-R）制定了不同的模拟视频信号到数字视频编码的标准，包括BT.601（用于SDTV），BT.709（用于HDTV），BT.2020（用于UHDTV）等，规定了RGB色彩空间到YCbCr色彩空间的计算方式。

A. BT.601计算方式

$$
\left[ \begin{array} { c } { Y ^ { \prime } } \\ { U } \\ { V } \end{array} \right] = \left[ \begin{array} { r r r } { 0.299 } & { 0.587 } & { 0.114 } \\ { - 0.14713 } & { - 0.28886 } & { 0.436 } \\ { 0.615 } & { - 0.51499 } & { - 0.10001 } \end{array} \right] \left[ \begin{array} { l } { R } \\ { G } \\ { B } \end{array} \right]
$$

B. BT.709计算方式

$$
\left[ \begin{array} { l } { Y ^ { \prime } } \\ { U } \\ { V } \end{array} \right] = \left[ \begin{array} { c c c } { 0.2126 } & { 0.7152 } & { 0.0722 } \\ { - 0.09991 } & { - 0.33609 } & { 0.436 } \\ { 0.615 } & { - 0.55861 } & { - 0.05639 } \end{array} \right] \left[ \begin{array} { l } { R } \\ { G } \\ { B } \end{array} \right]
$$

&emsp;&emsp;此外色度下采样（chroma subsampling）也具有不同的方式，根据U, V采样分量的不同，YUV具有不同的采样格式。主要包括YUV444（每一个Y对应一组UV分量）, YUV422（每两个Y共用一组UV分量）, YUV420（每四个Y共用一组UV分量）。

&emsp;&emsp;根据不同的存储格式，其又分为打包（packed）格式和平面（planar）格式。对于平面YUV格式，先连续存储所有像素点的Y，紧接着存储所有像素点的U，随后是所有像素点的V；对于打包YUV格式，每个像素点的Y, U, V是连续交叉存储的。

&emsp;&emsp;结合上述两种格式，实际使用中常见的有YV12，YU12格式、NV12、NV21格式等。

1. YV12，YU12格式：使用YUV420采样格式，使用平面存储格式（YYYYYYYY VV UU, YYYYYYYY UU VV）
2. NV12、NV21格式：使用YUV420采样格式，交叉使用平面存储格式与打包存储格式（YYYYYYYY UVUV, YYYYYYYY VUVU）

&emsp;&emsp;y4m文件格式在yuv文件的基础上使用头文件来存储视频规格。在原始的yuv序列的起始和每一帧的头部都加入了纯文字形式的视频参数信息，包括分辨率、帧率、逐行/隔行扫描方式、高宽比（aspect ratio）。y4m文件以一段明文开始，其前10个字符是署名”YUV4MPEG2 “（注意最后一个字符是空格（ASCII 0x20）。紧跟在署名后面的是各种视频参数信息，各参数信息都以空格（ASCII 0x20）分隔。每帧数据都以'FRAME'开始再在后面加一个0x0A，参数信息和帧头之间也有一个0x0A。

1. 帧宽：'W'后跟明文整数, 如'W720'；
2. 帧高：'H'后跟明文整数，如'H480'；
3. 帧率：'F'后跟两个整数的比，如'F30000:1001'；
4. 扫描方式：'I'后跟一个单独的字母表明交错的模式，如'Ip'（Progressive, 逐行）；
5. 宽高比：'A'后跟两个整数的比，如'A0:0'、'A4:3'；
6. 色彩空间：'C'后跟色彩空间采样格式，如'C420jpeg'、'C420'；
7. 注释：'X'开始，被解析器忽略。

<center>
<img src="https://i.loli.net/2019/01/05/5c3036bac4646.jpg" alt="20140219182620203.jpg" title="20140219182620203.jpg" />
</center>
<center  style="font-size:90%">
图3.2 y4m文件格式示意图
</center>

## 四、 视频解码方法

### 4.1 FFmpeg

&emsp;&emsp;使用FFmpeg编解码器命令行工具可以将视频文件解码为多帧图像：

```sh
ffmpeg -i video_name.xxx -f image2 image_name%04d.xxx -hide_banner
```
其中 `-i video_name.xxx` 参数用于指示输入视频文件名，`-f image2 image_name%04d.xxx` 参数用于指示输出图像文件名（%04d表示文件名以四位数字为后缀），`-hide_banner` 参数用于指示隐藏编译信息。

或特定一帧图像：

```sh
ffmpeg -i video_name.xxx -ss xx:xx:xx.xxx -vframes 1 -f image2 image_name.xxx
```
其中 `-i video_name.xxx` 参数用于指示输入视频文件名，`-ss xx:xx:xx.xxx` 用于指定开始处理帧的时间，`-vframes 1` 用于指定提取帧的个数，`-f image2 image_name.xxx` 参数用于指示输出图像文件名。

或按照一定时间间隔：

```sh
ffmpeg -i video_name.xxx -vf fps=1 -f image2 image_name%04d.xxx -hide_banner
```
其中 `-i video_name.xxx` 参数用于指示输入视频文件名，`-vf fps=1` 用于指定帧率为1的视频过滤器，`-f image2 image_name%04d.xxx` 参数用于指示输出图像文件名（%04d表示文件名以四位数字为后缀），`-hide_banner` 参数用于指示隐藏编译信息。

或提取所有关键帧：

```sh
ffmpeg -i video_name.xxx -vf "select=eq(pict_type\,I)" -vsync vfr -f image2 image_name%04d.xxx -hide_banner
```
其中 `-i video_name.xxx` 参数用于指示输入视频文件名，`-vf "select=eq(pict_type\,I)"` 用于指定选取所有关键帧的视频过滤器，`-vsync vfr` 用于指定过滤器视频的同步使用使用可变比特率，`-f image2 image_name%04d.xxx` 参数用于指示输出图像文件名（%04d表示文件名以四位数字为后缀），`-hide_banner` 参数用于指示隐藏编译信息。

### 4.2 Python

可以使用的Python库包括OpenCV，PyAV，MoviePy等。

A. OpenCV（顺序读取）:

```pyton
import cv2 

def FrameCapture(path):    
	vidObj = cv2.VideoCapture(path) 
	count = 0  
	success = 1
	while success: 
		success, image = vidObj.read() 
		cv2.imwrite("frame%d.jpg" % count, image) 
		count += 1 
```
B. PyAV（顺序读取）:

```pyton
import av
v = av.open(path)

for packet in container.demux():
    for frame in packet.decode():
        if frame.type == 'video':
            img = frame.to_image()  # PIL/Pillow image
            arr = np.asarray(img)  # numpy array
            # Do something!
```

C. pims（随机读取）:

```pyton
import pims
v = pims.Video(path)
v[-1]  # a 2D numpy array representing the last frame
```

D. moviepy（不适合大容量视频数据）:

```pyton
from moviepy.editor import VideoFileClip
myclip = VideoFileClip(path)
```

E. imageio（顺序读取）:

```pyton
import imageio
vid = imageio.get_reader(filename,  'ffmpeg')

for num, image in vid.iter_data():
    print(image.mean())

metadata = vid.get_meta_data()
```