---
layout: post
title: "文献对比"
description: "毕业设计参考文献技术对比"
categories: [清单]
tags: [参考文献]
redirect_from:
  - /2018/12/28/
---

> 站在巨人的肩膀上

* Kramdown table of contents
{:toc .toc}
# 文献对比
<div id="list"></div>
## 0 参考文献列表

| 方法简称  |                           论文名称                           |
| :-------: | :----------------------------------------------------------: |
|  VSRNet   |  Video Super-Resolution With Convolutional Neural Networks   |
| VSRNet-MC | End-to-End Learning of Video Super-Resolution with Motion Compensation |
|  VESPCN   | Real-time video super-resolution with spatio-temporal networks and motion compensation |
|   DRVSR   |         Detail-Revealing Deep Video Super-Resolution         |
|  DUFNet   | Deep Video Super-Resolution Network Using Dynamic Upsampling Filters Without Explicit Motion Compensation |

### 0.1 [Video Super-Resolution With Convolutional Neural Networks](#list)

Kappeler, Armin, et al. "[Video super-resolution with convolutional neural networks.](http://www.qiqindai.com/wp-content/uploads/2018/11/Video-Super-Resolution-With-Convolutional-Neural-Networks.pdf)" IEEE Transactions on Computational Imaging 2.2 (2016): 109-122.

Kappeler A, Yoo S, Dai Q, et al. Video super-resolution with convolutional neural networks[J]. IEEE Transactions on Computational Imaging, 2016, 2(2): 109-122.

### 0.2 End-to-End Learning of Video Super-Resolution with Motion Compensation

Makansi, Osama, Eddy Ilg, and Thomas Brox. "[End-to-end learning of video super-resolution with motion compensation.](https://arxiv.org/pdf/1707.00471)" German Conference on Pattern Recognition. Springer, Cham, 2017.

Makansi O, Ilg E, Brox T. End-to-end learning of video super-resolution with motion compensation[C]//German Conference on Pattern Recognition. Springer, Cham, 2017: 203-214.

### 0.3 Real-time video super-resolution with spatio-temporal networks and motion compensation

Caballero, Jose, et al. "[Real-Time Video Super-Resolution with Spatio-Temporal Networks and Motion Compensation.](http://openaccess.thecvf.com/content_cvpr_2017/papers/Caballero_Real-Time_Video_Super-Resolution_CVPR_2017_paper.pdf)" CVPR. Vol. 1. No. 2. 2017.

Caballero J, Ledig C, Aitken A P, et al. Real-Time Video Super-Resolution with Spatio-Temporal Networks and Motion Compensation[C]//CVPR. 2017, 1(2): 7.

### 0.4 Detail-Revealing Deep Video Super-Resolution

Tao, Xin, et al. "[Detail-revealing deep video super-resolution.](http://openaccess.thecvf.com/content_ICCV_2017/papers/Tao_Detail-Revealing_Deep_Video_ICCV_2017_paper.pdf)" Proceedings of the IEEE International Conference on Computer Vision, Venice, Italy. 2017.

Tao X, Gao H, Liao R, et al. Detail-revealing deep video super-resolution[C]//Proceedings of the IEEE International Conference on Computer Vision, Venice, Italy. 2017: 22-29.

### 0.5 Deep Video Super-Resolution Network Using Dynamic Upsampling Filters Without Explicit Motion Compensation

Jo, Younghyun, et al. "[Deep Video Super-Resolution Network Using Dynamic Upsampling Filters Without Explicit Motion Compensation.](http://openaccess.thecvf.com/content_cvpr_2018/papers/Jo_Deep_Video_Super-Resolution_CVPR_2018_paper.pdf)" Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition. 2018.

Jo Y, Oh S W, Kang J, et al. Deep Video Super-Resolution Network Using Dynamic Upsampling Filters Without Explicit Motion Compensation[C]//Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition. 2018: 3224-3232.

## 1 主要技术特点

| 方法简称  |                           技术特点                           |
| :-------: | :----------------------------------------------------------: |
|  VSRNet   |  CLG-TV+AMC+CNN   |
| VSRNet-MC | FlowNet2-SD+CNN, joint train |
|  VESPCN   | MCT+ESPCN |
|   DRVSR   | MCT+SPMC+DF-Net(Detail Fusion Net, encoder-decoder) |
|  DUFNet   | DUF+Resdiual, no motion compensation |

## 2 运动估计方法

| 方法简称  |                           运动补偿方法                           |
| :-------: | :----------------------------------------------------------: |
|  VSRNet   |  CLG-TV+AMC   |
| VSRNet-MC | FlowNet2-SD+JUBW |
|  VESPCN   | MCT(Motion Compensation Transformer) |
|   DRVSR   | MCT+SPMC(Sub-pixel Motion Compensation) |
|  DUFNet   | No|

## 3 数据集

| 方法简称  |                           数据集                           |
| :-------: | :----------------------------------------------------------: |
|  VSRNet   |  Train: Myanmar video sequence; Test: Videoset4   |
| VSRNet-MC | Train: Myanmar video sequence; Test: Videoset4 |
|  VESPCN   | Train: CDVL, 115 uncompressed full HD videos; Test: Videoset4 |
|   DRVSR   | Train: 975 sequences; Test: Videoset4 |
|  DUFNet   | Train: 351 videos; Test: Videoset4 |