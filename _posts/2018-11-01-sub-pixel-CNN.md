---
layout: post
title: "高效亚像素卷积网络 ESPCN"
description: "使用高效亚像素卷积神经网络的实时单图像和视频超分辨率"
categories: [计算机视觉]
tags: [翻译, 超分辨]
redirect_from:
  - /2018/11/01/
---

> 近年来，几种基于深度神经网络的模型在单幅图像超分辨率重建的精度和计算性能方面取得了巨大成功。这些方法在重建之前使用单个滤波器（通常是双三次插值）将低分辨率（LR）输入图像放大到高分辨率（HR）空间。这意味着超分辨率（SR）操作在HR空间中执行。我们证明了这是次优的并且增加了计算复杂性。我们在本文提出了第一个能够在单个K2 GPU上实现1080p视频实时SR的卷积神经网络（CNN）。为此，我们提出了一种新的CNN架构，其在LR空间提取特征图。此外，我们还引入了一个高效的亚像素卷积层，它可以学习一系列升频滤波器，将最终的LR特征图放大到HR输出。通过这种方法，我们有效地将SR管道中的手工双三次滤波器替换为针对每个特征图专门训练的更复杂的放大滤波器，同时还降低了整个SR操作的计算复杂度。我们使用公开数据集中的图像和视频来评估所提出的方法，结果表明其明显表现更好（图像上提高了0.15dB，视频上提高了0.39dB），并且比以往的基于CNN的方法快一个数量级。

* Kramdown table of contents
{:toc .toc}

# 高效亚像素卷积神经网络

## 1 引言

从对应的低分辨率数据复原高分辨率图像或视频是数字图像处理领域一个非常吸引人的主题。这个任务通常称为超分辨（super-resolution, SR），在许多领域有着直接的应用，比如高清晰度电视，医学图像，遥感图像，人脸识别以及监视等。SR问题假设LR数据是HR数据的低通滤波（模糊）、下采样并且加上噪声的版本。由于在不可逆的低通滤波和下采样过程中高频信息的丢失，使其成为一个高度病态的问题。此外，SR操作实际上是LR空间到HR空间的一对多映射，其可以有多种解，而确定正确的解是非平凡的。许多SR技术的基本假设是大部分高频数据是冗余的，因此可以从低频分量精确重建。因此，SR是一个推理问题，依赖于问题图像中的统计模型。

许多方法假设一些图像是相同场景不同视角下的LR实例，比如进行了唯一的仿射变换。这些方法被归类为多图像超分辨率（MISR）方法，通常使用附加信息限制病态问题并尝试反转下采样过程来利用*显式冗余*（explicit redundancy）。然而，这些方法需要复杂的图像配准和融合阶段，不仅消耗大量计算，其准确性也直接影响重建图像结果的质量。另一类方法是单图像超分辨率（SISR）技术。这些方法尝试学习自然数据中存在的隐式冗余，从单个LR实例恢复丢失的HR信息。这通常表现在图像的空间局部相关性和和视频附加的时间相关性。这种情况下，需要先验信息以重建约束的形式来限制解的重建空间。

### 1.1 相关工作

SISR方法的目的是从单幅LR图像复原一个HR图像。最近流行的SISR方法可以分类为基于边缘信息方法、基于图像统计方法以及基于块信息方法等。[“Single-image super-resolution: A benchmark”](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.677.4639&rep=rep1&type=pdf)对更通用的SISR方法进行了详细的综述。最近一系列基于稀疏性技术的方法在处理SISR问题方面得到蓬勃发展。稀疏编码是一种有效的机制，其假设任何自然图像可以在一个变换域中稀疏地表示。这个变换域通常是图像原子的字典，其可以通过尝试发现LR和HR块之间对应关系的训练过程来学习。该字典能够嵌入必要的先验知识来约束超分辨率病态问题中不可见的数据。基于稀疏性技术的缺点是通过非线性重建引入稀疏性约束通常在计算上是昂贵的。

最近，通过神经网络导出图像模型的方法已经显示出SISR的希望。这些方法采用反向传播算法在大型图像数据库如ImageNet上进行训练，以学习LR和HR图像块的非线性映射。[“Deep network cascade for image super-resolution”](https://pdfs.semanticscholar.org/9033/b93814aaf7f94f190eea995da4a5664c92ff.pdf)使用堆叠的协作局部自动编码器逐层超解析LR图像。[“Image super-resolution with fast approximate convolutional sparse coding”](https://mediatum.ub.tum.de/doc/1281592/file.pdf)提出了一种基于预测卷积稀疏编码框架扩展的SISR方法。[“Image super-resolution using deep convolutional networks”](https://arxiv.org/pdf/1501.00092)提出了一种受稀疏编码方法启发的多层卷积神经网络（CNN）。[“Trainable nonlinear reaction diffusion: A flexible framework for fast and effective image restoration”](https://arxiv.org/pdf/1508.02848)提出使用多级可训练非线性反应扩散（TNRD）作为CNN的替代，其中权重和非线性是可训练的。[“Learning fast approximations of sparse coding”](https://dl.acm.org/citation.cfm?id=3104374)提出了一个端到端的级联稀疏编码网络，以充分利用图像的自然稀疏性。网络结构不限于神经网络，[“Fast and accurate image upscaling with super-resolution forests”](https://www.cv-foundation.org/openaccess/content_cvpr_2015/papers/Schulter_Fast_and_Accurate_2015_CVPR_paper.pdf)使用了随机森林。

### 1.2 动机和改进

随着CNN的发展，算法的效率，特别是其计算和内存消耗，变得越来越重要。与以前手工制作的模型相比，深度网络模型学习非线性关系的灵活性已被证明可以获得更高的重建精度。为了将LR图像超解析到HR空间，有必要在某些情况下增加LR图像的分辨率以匹配HR图像的分辨率。

[“Image super-resolution with fast approximate convolutional sparse coding”](https://mediatum.ub.tum.de/doc/1281592/file.pdf)中图像分辨率在网络中逐渐提高。另外一种流行的方法是在网络的第一层或之前增加图像分辨率。然而这种方法有一系列的缺点。首先，在图像增强步骤之前增加LR图像的分辨率会导致计算复杂性的提高。这对于卷积网络尤其是个问题，其处理速度直接取决于输入图像分辨率。其次，通常用于完成任务的插值方法，如双三次插值，不会带来额外的信息来解决病态的重建问题。

[“Image super-resolution using deep convolutional networks”](https://arxiv.org/pdf/1501.00092)在角注中简要介绍了升频滤波器的学习。然而，作为SR操作的一部分将其整合到CNN中的重要性未得到充分认识，并且没有对其进行深入探索。此外，正如文章所言，由于缺少输像convnet一样得到良好优化实现的尺寸大于输入尺寸的卷积层，也不允许这样的行为。

在本文中，不同于之前的工作，我们建议仅在网络的最末端将分辨率从LR增加到HR，并从LR特征图中超解析HR数据。这消除了完成大部分SR操作中使用过高的HR分辨率的需要。为此，我们提出了一种更有效的亚像素卷积层，用来学习图像和视频超分辨率的放大操作。

<center>
<img src="https://i.endpot.com/image/Y0NUT/ESPCN.PNG" alt="ESPCN.PNG" title="ESPCN.PNG" />
</center>
<center style="font-size:90%">
图1 ESPCN
</center>

这些改进的优点是双重的。

- 在我们的网络中，放大由网络的最后一层进行处理。这意味着每个LR图像直接输入到网络，并通过LR空间中的非线性卷积进行特征提取。由于输入分辨率降低，我们可以有效地使用较小的滤波器尺寸来整合相同的信息，同时保持给定的上下文区域。 分辨率和滤波器尺寸的减小大大降低了计算和内存复杂度，足以实现高分辨率（HD）视频的超分辨率，如第3.5节所示。
- 对于 $$ L $$ 层的网络，我们学习用于 $$ n_{L-1} $$ 特征映射的 $$ n_{L-1} $$ 升频滤波器，而不是原始输入图像的一个升频滤波器。此外，不使用显式插值滤波器意味着网络隐含地学习SR所需的处理。因此，与在第一层处的单个固定滤波器放大相比，该网络能够学习更好更复杂的LR到HR映射。这也引起了模型的重建精度的额外增益，如第3.3.2和3.4节所示。

我们使用公开可用的基准数据集的图像和视频验证了所提出的方法，并将我们的性能与之前的工作进行了比较。结果表明，我们所提出的模型取得了先进的性能，并且比以前发表的图像和视频方法快了近一个数量级。

<center>
<img src="https://i.endpot.com/image/UGZQS/run_times.PNG" alt="run_times.PNG" title="run_times.PNG" />
</center>
<center style="font-size:90%">
图2 不同方法运行时间和精确度，放大因子为3
</center>

## 2 方法

SISR的任务是从给定LR图像 $$ \bold{I}^{LR} $$ 来估计一个HR图像 $$ \bold{I}^{SR} $$。其中，$$ \bold{I}^{LR} $$ 是从一个相关的原始HR图像 $$ \bold{I}^{HR} $$ 下采样得到。下采样操作是确定并且已知的：为了从 $$ \bold{I}^{HR} $$ 产生 $$ \bold{I}^{LR} $$，首先使用高斯滤波器对 $$ \bold{I}^{HR} $$ 进行卷积，从而模拟相机的点扩散函数，然后使用因子 $$ r $$ 对图像进行下采样。我们将 $$ r $$ 称为上采样比例。通常，$$ \bold{I}^{LR} $$ 和 $$ \bold{I}^{HR} $$ 都具有  $$ C $$ 个颜色通道，因此，它们可以分别表示为尺寸为 $$ H \times W \times C $$ 和 $$ rH \times rW \times rC $$ 的实值张量。

为了解决SISR问题，SRCNN从 $$ \bold{I}^{LR} $$ 的一个放大插值版本而不是原始 $$ \bold{I}^{LR} $$ 进行复原。为了复原 $$ \bold{I}^{SR} $$，使用了一个三层的卷积网络。在这一节，我们提出了一个新的网络结构，如图1所示，来避免 $$ \bold{I}^{LR} $$ 进入网络前的放大操作。在我们的结构中，首先对LR图像直接应用了一个 $$ l $$ 层的卷积神经网络，接着应用了一个亚像素卷积层来对LR特征图进行放大来产生 $$ \bold{I}^{SR} $$。

对于一个 $$ L $$ 层的网络，前 $$ L-1 ​$$ 层可以描述如下：

$$
\tag{1} f^1(\bold{I}^{LR}; W_1, b_1) = \phi(W_1 * \bold{I}^{LR} + b_1)
$$

$$
\tag{2} f^l(\bold{I}^{LR}; W_{1:l}, b_{1:l}) = \phi(W_l * f^{l-1}(\bold{I}^{LR}) + b_l)
$$

其中 $$ W_l,b_l,l \in (1, L-1) $$ 分别是学习的网络权重和偏置。$$ W_l $$ 是一个大小为 $$ n_{l-1} \times n_l \times k_l \times k_l $$ 的二维张量，其中 $$ n_l $$ 是 $$ l $$ 层的特征数量，$$ n_0 = C $$ ，$$ k_l $$ 是 $$ l $$ 层的滤波器尺寸。偏置 $$ b_l $$ 是长度为 $$ n_l $$ 的向量。非线性函数（或激活函数） $$ \phi $$ 按元素应用并固定。最后一层 $$ f^L $$ 必须将LR特征图转换为HR图像 $$ \bold{I}^{HR} $$。

### 2.1 反卷积层

添加反卷积层是从最大池化层和其他图像下采样层恢复分辨率的流行选择。该方法已成功用于可视化层激活以及使用网络的高级特征生成语义片段。SRCNN中使用的双三次插值是反卷积层的一个特例。

另一种放大LR图像的方法是在LR空间使用 $$ \frac 1 r $$ 小数步幅的卷积，可以简单地通过插值、穿孔或者展开等方法从LR空间映射到HR空间，再接着在HR空间进行一层步幅为1的卷积来实现。然而这些实现将计算成本增加了 $$ r^2 $$ 倍，因为卷积发生在HR空间。

此外，在LR空间中步幅为 $$ \frac 1 r $$，使用大小为 $$ k_s $$，权重间距为 $$ \frac 1 r $$ 的滤波器 $$ W_s $$ 会激活用于卷积的 $$ W_s $$ 的不同部分。在像素之间的权重不会被激活，也不需要计算。激活模式的数量正好是 $$ r^2 $$，根据其位置，每个模式最多有 $$ {\lceil \frac{k_s}{r} \rceil}^2 $$ 权重被激活。滤波器在图像上卷积过程期间，根据不同的亚像素位置，这些模式周期性地被激活：$$ \bmod(x, r), \bmod(y, r) $$ 其中 $$ x, y $$ 代表HR空间输出像素的坐标。在本文中，我们提出了一种当 $$ \bmod(k_s, r) = 0 $$ 时实现上述操作的高效方法：

$$
\tag{3} \mathbf { I } ^ { S R } = f ^ { L } \left( \mathbf { I } ^ { LR } \right) = \mathcal { P } \mathcal { S } \left( W _ { L } * f ^ { L - 1 } \left( \mathbf { I } ^ { LR } \right) + b _ { L } \right)
$$

其中，$$ \mathcal{PS} $$ 是一个周期性的混洗算子，它将大小为 $$ H \times W \times C \cdot r^2 $$ 的张量重新排列为 $$ rH \times rW \times C $$  的形状。该操作的效果如图1所示。在数学上，该操作可以用下面的方式描述：

$$
\tag{4} \mathcal { P S } ( T ) _ { x , y , c } = T _ { \lfloor x / r \rfloor , \lfloor y / r \rfloor , c \cdot r \cdot \rm{mod}( y , r ) + c \cdot \rm{mod}( x , r ) }
$$

卷积算子 $$ W_L $$ 形状为 $$ n_{L-1} \times r^2C \times k^L \times k^L $$。请注意，我们不会在最后一层对卷积的输出应用非线性。容易看出，当 $$ k_L = \frac {k_s} {r} $$ 并且 $$ \mod(k_s, r) = 0 $$ 时，等价于LR空间中使用滤波器 $$ W_s  $$ 的亚像素卷积层。我们将新网络层称为亚像素卷积层，我们的网络称为高效亚像素卷积神经网络（ESPCN）。最后一层直接从LR特征图通过每个特征图所对应的放大滤波器生成HR图像，如图4所示。

给定训练集，其由HR样本图像 $$ \mathbf{I}_n^{HR}, n = 1 \dots N $$，我们生成对应的LR图像 $$ \mathbf{I}_n^{LR}, n = 1 \dots N $$，并计算重建图像的像素均方误差（MSE）作为训练网络的目标函数：

$$
\tag{5} \ell \left( W _ { 1 : L } , b _ { 1 : L } \right) = \frac { 1 } { r ^ { 2 } H W } \sum _ { x = 1 } ^ { r H } \sum _ { x = 1 } ^ { r W } \left( \mathbf { I } _ { x , y } ^ { H R } - f _ { x , y } ^ { L } \left( \mathbf { I } ^ { L R } \right) \right) ^ { 2 }
$$

值得注意的是，与HR空间中的压缩或卷积相比，上述周期性混洗可以非常快实现，因为每个操作是独立的，在一个周期中可以简单地并行化。 因此，与在前向通道中的去卷积层相比，我们提出的层快了 $$ \log_2{r^2} $$ 倍，并且与在卷积之前使用各种形式放大的实现相比，快了 $$ r^2 $$ 倍。

<center>
<img src="https://i.endpot.com/image/WWW3H/fig3.PNG" alt="fig3.PNG" title="fig3.PNG" />
</center>
<center style="font-size:90%">
图3 在ImageNet上训练得到的第一层滤波器，放大因子为3，按方差排序
</center>

<center>
<img src="https://i.endpot.com/image/VLSRD/fig4.PNG" alt="fig4.PNG" title="fig4.PNG" />
</center>
<center style="font-size:90%">
图4 在ImageNet上训练得到的最后一层滤波器，放大因子为3 (a)表示SRCNN中的9-5-5模型的权重 (b)表示ESPCN模型的权重 (c) 表示(b)经过亚像素卷积操作后的权重 默认顺序
</center>

## 3 实验

<center>
<img src="https://i.endpot.com/image/KSQ6R/fig5.PNG" alt="fig5.PNG" title="fig5.PNG" />
</center>
<center style="font-size:90%">
图5 来自Set14的“Baboon”，“Comic”和“Monarch”的超分辨率示例，放大因子为3，子图下方显示PSNR值
</center>

补充材料中提供了定量评估的详细报告，包括原始数据，包括图像和视频，下采样数据，超分辨率数据，总体和单独得分以及在K2 GPU上的运行时间。

### 3.1 数据集

在评估过程中，我们使用了公开可用的基准数据集，包括SISR论文广泛使用的[Timofte数据集](https://pdfs.semanticscholar.org/ca57/66b91da4903ad6f6d40a5b31a3ead1f7f6de.pdf)，包含91个训练图像和两个测试数据集Set5和Set14，分别提供5和14张图像。[伯克利分割数据集](http://digitalassets.lib.berkeley.edu/techreports/ucb/text/CSD-01-1133.pdf)BSD300和BSD500分别提供了100和200个图像用于测试，[超级纹理数据集](https://pdfs.semanticscholar.org/fc3a/e73f825259131c2259d0f0fab435594a9e69.pdf)提供了136个纹理图像。对于我们的最终模型，我们使用来自[ImageNet](https://arxiv.org/pdf/1409.0575)的50,000个随机选择的图像进行训练。 在之前的工作之后，我们在本节只考虑YCbCr色彩空间中的亮度通道，因为人类对亮度变化更敏感。对于每个放大因子，我们都会训练一个特定的网络。

对于视频实验，我们使用来自公开的[Xiph数据库](https://media.xiph.org/video/derf/)的1080p高清视频。 该数据库包含8个高清视频的集合，长度约为10秒，宽度和高度为1920×1080。此外，我们还使用[Ultra Video Group数据库](http://ultravideo.cs.tut.fi/)，其中包含7个1920×1080大小时长为5s的视频。

### 3.2 实现细节

### 3.3 图像超分辨率结果

#### 3.3.1 亚像素卷积层的作用

#### 3.3.2 与先进方法的对比

### 3.4 视频超分辨率结果

### 3.5 运行时间评估

## 4 结论

## 5 展望