---
layout: post
title: "光流"
description: "光流的概念及其估计方法"
categories: [计算机视觉]
tags: [翻译]
redirect_from:
  - /2018/10/29/
---

> 光流是视觉领域中对于观察者与场景之间**相对运动**所造成的被观察物体发生明显变化的概念[^1]

* Kramdown table of contents
{:toc .toc}

# 光流

_文章原文来自 [Handbook of Mathematical Models in Computer Vision](http://www.cs.toronto.edu/~fleet/research/Papers/flowChapter05.pdf), David J. Fleet & Yair Weiss (2006)中的第五章节 "Optical Flow Estimation", 翻译时有所删减与修改_

## 引言

运动是世界的内在属性，也是我们视觉体验的一个组成部分。它是信息的丰富来源，能够支持多种视觉任务，包括3D形状获取、对象识别以及场景理解等。美国心理学家詹姆斯·吉布森 (James J. Gibson)[^2]在20世纪40年代引入了光流的概念来描述促使动物进行<strike>迁徙</strike>的视觉刺激。吉布森强调了视觉流动对可供性感知，以及辨别环境中行为可能性能力的重要性。吉布森的追随者和他的心理学生态学方法进一步证明了光流刺激对于观察者对物体的形状、距离和运动的感知能力所起到的重要作用。

## 光流的基本概念

假设在观察中，像素强度从一帧图像转移至后一帧图像：

$$
\tag{1.1} I(\vec{x}, t) = I(\vec{x} + \vec{u}, t + 1)
$$

其中，$$ I(\vec{x}, t) ​$$ 代表空间函数 $$ \vec{x} = (x, y)^T ​$$ 和时间 $$ t ​$$ 对应的图像强度；$$ \vec{u} = (u_1, u_2)^T ​$$ 代表二维空间速度。尽管_亮度恒定_ (brightness constancy) 很少完全成立，潜在的假设认为表面亮度在前后两帧图像间保持固定，其可以由理想条件得到：场景被限制为仅包含朗伯曲面 (Lambertian surface, 没有镜面反射)；具有远点光源；没有物体旋转，也没有二次照明。

为了导出二维速度 $$ \vec{u} $$，首先考虑一维的情况。假设 $$ f_1(x) $$ 和 $$ f_2(x) $$ 是两个时刻下的一维信号。如图1所示，假设 $$ f_2(x) $$ 由 $$ f_1(x) $$ 平移得到，则 $$ f_2(x) = f_1(x - d) $$ 其中 $$ d $$ 代表平移量。$$ f_1(x - d) $$ 关于 $$ x $$ 泰勒级数展开得：

$$
\tag{1.2} f_1(x - d) = f_1(x) - df_1'(x) + O(d^2f_1'')
$$

其中 $$ f' \equiv df(x)/dx $$。从而可以将位于 $$ x $$ 的两个信号之差记为

$$
f_1(x) - f_2(x) = df_1'(x) + O(d^2f_1'')
$$

忽略二阶和更高阶项，可以得到 $$ d $$ 的近似值：

$$
\tag{1.3} \hat{d} = \frac {f_1(x) - f_2(x)} {f_1'(x)}
$$

<center>
<img src="https://i.endpot.com/image/8RLZ1/1540820796878.png" alt="Fig1.png" title="Fig1.png" />
</center>
<center style="font-size:90%">图1 梯度约束将信号的位移与其时间差和空间导数（斜率）相关联</center>

一维的情况可以直接扩展到二维。如上所述，假设位移图像可以由一阶泰勒级数近似：

$$
\tag{1.4} I(\vec{x} + \vec{u}, t+1) \approx I(\vec{x}, t) + \vec{u} \cdot \nabla I(\vec{x}, t) + I_t(\vec{x}, t)
$$

其中 $$ \nabla I \equiv (I_x, I_y) $$ 和 $$ I_t $$ 分别代表图像 $$ I $$ 关于空间和时间的偏导数，$$ \vec{u} = (u_1, u_2)^T ​$$ 代表二维速度。忽略泰勒级数的高阶项，将线性近似代入公式1.1，得到：

$$
\tag{1.5} \nabla I(\vec{x}, t) \cdot \vec{u} + I_t(\vec{x}, t) = 0
$$

公式1.5将速度与图像某一位置的时空导数相关联，通常称为**梯度约束方程**(gradient constraint equation)。如果只能够获得两帧图像，或者不能估算 $$ I_t $$，可以直接推导出一个紧密相关的梯度约束，公式1.5中的 $$ I_t(\vec{x}, t) $$ 根据 $$ \delta I(\vec{x}, t) \equiv I(\vec{x}, t+1) - I(\vec{x}, t) $$ 进行替换。

对于恒定亮度的点的追踪可以被视为强度恒定的二维路径 $$ \vec{x}(t) $$ 的估计：

$$
\tag{1.6} I(\vec{x}(t), t) = c
$$

可以得到关于时间的导数为：

$$
\tag{1.7} \frac d {dt} I(\vec{x}(t), t) = 0
$$

将公式1.7左边展开，使用链式法则可以得到：

$$
\tag{1.8} \frac d {dt} I(\vec{x}(t), t) = \frac {\partial I} {\partial x} \frac {\partial x} {\partial t} + \frac {\partial I} {\partial y} \frac {\partial y} {\partial t} + \frac {\partial I} {\partial t} \frac {\partial t} {\partial t} = \nabla I \cdot \vec{u} + I_t
$$

其中路径导数就代表光流 $$ \vec{u} \equiv (dx/dt, dy/dt)^T $$。

[^1]: https://en.wikipedia.org/wiki/Optical_flow
[^2]: https://en.wikipedia.org/wiki/James_J._Gibson

