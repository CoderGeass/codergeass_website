---
layout: post
title: "视觉世界的感知"
description: "光流的概念及其估计方法"
categories: [计算机视觉]
tags: [创作中, 翻译]
redirect_from:
  - /2018/10/30/
---

> 光流是视觉领域中对于观察者与场景之间**相对运动**所造成的被观察物体发生明显变化的概念[^1]

* Kramdown table of contents
{:toc .toc}
# 视觉世界的感知

_文章原文来自 [Handbook of Mathematical Models in Computer Vision](http://www.cs.toronto.edu/~fleet/research/Papers/flowChapter05.pdf), David J. Fleet & Yair Weiss (2006)中的第五章节"Optical Flow Estimation"。翻译时有所删减与修改_

## 引言

运动是世界的内在属性，也是我们视觉体验的一个组成部分。它是信息的丰富来源，能够支持多种视觉任务，包括3D形状获取、对象识别以及场景理解等。美国心理学家詹姆斯·吉布森(James J. Gibson)[^2]在20世纪40年代引入了光流的概念来描述促使动物进行迁徙的视觉刺激。吉布森强调了视觉流动对可供性感知的重要性，以及辨别环境中行动可能性的能力。



[^1]: https://en.wikipedia.org/wiki/Optical_flow