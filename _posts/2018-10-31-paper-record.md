---
layout: post
title: "文献记录"
description: "毕业设计参考文献阅读记录"
categories: [清单]
tags: [参考文献]
redirect_from:
  - /2018/10/31/
---

> 站在巨人的肩膀上

* Kramdown table of contents
{:toc .toc}

# 文献记录

## 0 数字图像处理

### 0.1 书籍

#### 0.1.1 Going deeper with convolutions

Petrou, Maria, and Costas Petrou. Image processing: the fundamentals. John Wiley & Sons, 2010.

Petrou M, Petrou C. Image processing: the fundamentals[M]. John Wiley & Sons, 2010.

## 1 深度学习

### 1.1 书籍

### 1.2 论文

#### 1.2.1 Going deeper with convolutions

Szegedy, Christian, et al. "[Going deeper with convolutions.](https://www.cv-foundation.org/openaccess/content_cvpr_2015/papers/Szegedy_Going_Deeper_With_2015_CVPR_paper.pdf)" Proceedings of the IEEE conference on computer vision and pattern recognition. 2015.

Szegedy C, Liu W, Jia Y, et al. Going deeper with convolutions[C]//Proceedings of the IEEE conference on computer vision and pattern recognition. 2015: 1-9.

#### 1.2.3 Spatial Transformer Networks

Jaderberg, Max, Karen Simonyan, and Andrew Zisserman. "[Spatial transformer networks.](http://papers.nips.cc/paper/5854-spatial-transformer-networks.pdf)" Advances in neural information processing systems. 2015.

Jaderberg M, Simonyan K, Zisserman A. Spatial transformer networks[C]//Advances in neural information processing systems. 2015: 2017-2025.

## 2 运动估计

### 2.1 书籍

#### 2.1.1 Handbook of mathematical models in computer vision

Fleet, David, and Yair Weiss. "[Optical flow estimation.](http://www.cs.toronto.edu/~fleet/research/Papers/flowChapter05.pdf)" Handbook of mathematical models in computer vision. Springer, Boston, MA, 2006. 237-257.

Fleet D, Weiss Y. Optical flow estimation[M]//Handbook of mathematical models in computer vision. Springer, Boston, MA, 2006: 237-257.

#### 2.1.2 About direct methods

Irani, Michal, and P. Anandan. "[About direct methods.](https://pdfs.semanticscholar.org/3d18/95f35202c2f421491df10105ff83c851ebd1.pdf)" International Workshop on Vision Algorithms. Springer, Berlin, Heidelberg, 1999.

Irani M, Anandan P. About direct methods[C]//International Workshop on Vision Algorithms. Springer, Berlin, Heidelberg, 1999: 267-277.

#### 2.1.3 Feature Based Methods for Structure and Motion Estimation

Torr, Philip HS, and Andrew Zisserman. "[Feature based methods for structure and motion estimation.](https://www.researchgate.net/profile/Philip_Torr/publication/225775897_Feature_Based_Methods_for_Structure_and_Motion_Estimation/links/53d818df0cf2a19eee814d8f/Feature-Based-Methods-for-Structure-and-Motion-Estimation.pdf)" International workshop on vision algorithms. Springer, Berlin, Heidelberg, 1999.

Torr P H S, Zisserman A. Feature based methods for structure and motion estimation[C]//International workshop on vision algorithms. Springer, Berlin, Heidelberg, 1999: 278-294.

### 2.2 论文

#### 2.2.1 Determining Optical Flow

Horn, Berthold KP, and Brian G. Schunck. "[Determining optical flow.](https://dspace.mit.edu/bitstream/handle/1721.1/6337/%EE%80%80AIM%EE%80%81-572.pdf?sequence=2)" Artificial intelligence 17.1-3 (1981): 185-203.

Horn B K P, Schunck B G. Determining optical flow[J]. Artificial intelligence, 1981, 17(1-3): 185-203.

#### 2.2.2 A database and evaluation methodology for optical flow

Baker, Simon, et al. "[A database and evaluation methodology for optical flow.](https://link.springer.com/content/pdf/10.1007/s11263-010-0390-2.pdf)" International Journal of Computer Vision 92.1 (2011): 1-31.

Baker S, Scharstein D, Lewis J P, et al. A database and evaluation methodology for optical flow[J]. International Journal of Computer Vision, 2011, 92(1): 1-31.

#### 2.2.3 FlowNet: Learning Optical Flow with Convolutional Networks

Dosovitskiy, Alexey, et al. "[Flownet: Learning optical flow with convolutional networks.](https://www.cv-foundation.org/openaccess/content_iccv_2015/papers/Dosovitskiy_FlowNet_Learning_Optical_ICCV_2015_paper.pdf)" Proceedings of the IEEE International Conference on Computer Vision. 2015.

Dosovitskiy A, Fischer P, Ilg E, et al. Flownet: Learning optical flow with convolutional networks[C]//Proceedings of the IEEE International Conference on Computer Vision. 2015: 2758-2766.

#### 2.2.4 Unsupervised convolutional neural networks for motion estimation

Ahmadi, Aria, and Ioannis Patras. "[Unsupervised convolutional neural networks for motion estimation.](https://arxiv.org/pdf/1601.06087)" Image Processing (ICIP), 2016 IEEE International Conference on. IEEE, 2016.

Ahmadi A, Patras I. Unsupervised convolutional neural networks for motion estimation[C]//Image Processing (ICIP), 2016 IEEE International Conference on. IEEE, 2016: 1629-1633.

#### 2.2.5 Optical flow estimation using a spatial pyramid network

Ranjan, Anurag, and Michael J. Black. "[Optical flow estimation using a spatial pyramid network.](http://openaccess.thecvf.com/content_cvpr_2017/papers/Ranjan_Optical_Flow_Estimation_CVPR_2017_paper.pdf)" IEEE Conference on Computer Vision and Pattern Recognition (CVPR). Vol. 2. IEEE, 2017.

Ranjan A, Black M J. Optical flow estimation using a spatial pyramid network[C]//IEEE Conference on Computer Vision and Pattern Recognition (CVPR). IEEE, 2017, 2: 2.

#### 2.2.6 Video Enhancement with Task-Oriented Flow

Xue, Tianfan, et al. "[Video Enhancement with Task-Oriented Flow.](https://arxiv.org/pdf/1711.09078)" arXiv preprint arXiv:1711.09078 (2017).

Xue T, Chen B, Wu J, et al. Video Enhancement with Task-Oriented Flow[J]. arXiv preprint arXiv:1711.09078, 2017.


## 3 超分辨

### 3.0 综述

#### 3.0.1 Super-resolution: a comprehensive survey

Nasrollahi, Kamal, and Thomas B. Moeslund. "[Super-resolution: a comprehensive survey.](https://www.researchgate.net/profile/Thomas_Moeslund/publication/272031125_Super-resolution_A_comprehensive_survey/links/570f773308ae38897ba10bb3/Super-resolution-A-comprehensive-survey.pdf)" Machine vision and applications 25.6 (2014): 1423-1468.

Nasrollahi K, Moeslund T B. Super-resolution: a comprehensive survey[J]. Machine vision and applications, 2014, 25(6): 1423-1468.

#### 3.0.2 Super-resolution image reconstruction: A technical overview

Park, Sung Cheol, Min Kyu Park, and Moon Gi Kang. "[Super-resolution image reconstruction: a technical overview.](http://www.academia.edu/download/43777607/Super-Resolution_Image_Reconstruction_A_Technical_Overview.pdf)" IEEE signal processing magazine 20.3 (2003): 21-36.

Park S C, Park M K, Kang M G. Super-resolution image reconstruction: a technical overview[J]. IEEE signal processing magazine, 2003, 20(3): 21-36.

#### 3.0.3 Multimedia super-resolution via deep learning: A survey

Hayat, Khizar. "Multimedia super-resolution via deep learning: a survey." Digital Signal Processing (2018).

Hayat K. Multimedia super-resolution via deep learning: a survey[J]. Digital Signal Processing, 2018.

#### 3.0.4 Deep Learning for Single Image Super-Resolution: A Brief Review

Yang, Wenming, et al. "[Deep learning for single image super-resolution: A brief review.](https://arxiv.org/pdf/1808.03344)" arXiv preprint arXiv:1808.03344 (2018).

Yang W, Zhang X, Tian Y, et al. Deep learning for single image super-resolution: A brief review[J]. arXiv preprint arXiv:1808.03344, 2018.

### 3.1 单幅图像

#### 3.1.1 Image Super-Resolution Using Deep Convolutional Networks

Dong, Chao, et al. "[Image super-resolution using deep convolutional networks.](https://arxiv.org/pdf/1501.00092)" IEEE transactions on pattern analysis and machine intelligence 38.2 (2016): 295-307.

Dong C, Loy C C, He K, et al. Image super-resolution using deep convolutional networks[J]. IEEE transactions on pattern analysis and machine intelligence, 2016, 38(2): 295-307.

#### 3.1.2 Real-Time Single Image and Video Super-Resolution Using an Efficient Sub-Pixel Convolutional Neural Network

Shi, Wenzhe, et al. "[Real-time single image and video super-resolution using an efficient sub-pixel convolutional neural network.](https://www.cv-foundation.org/openaccess/content_cvpr_2016/papers/Shi_Real-Time_Single_Image_CVPR_2016_paper.pdf)" Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition. 2016.

Shi W, Caballero J, Huszár F, et al. Real-time single image and video super-resolution using an efficient sub-pixel convolutional neural network[C]//Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition. 2016: 1874-1883.


### 3.2 多幅图像/视频

#### 3.2.1 Video Super-Resolution With Convolutional Neural Networks

Kappeler, Armin, et al. "[Video super-resolution with convolutional neural networks.](http://www.qiqindai.com/wp-content/uploads/2018/11/Video-Super-Resolution-With-Convolutional-Neural-Networks.pdf)" IEEE Transactions on Computational Imaging 2.2 (2016): 109-122.

Kappeler A, Yoo S, Dai Q, et al. Video super-resolution with convolutional neural networks[J]. IEEE Transactions on Computational Imaging, 2016, 2(2): 109-122.

#### 3.2.2 End-to-End Learning of Video Super-Resolution with Motion Compensation

Makansi, Osama, Eddy Ilg, and Thomas Brox. "[End-to-end learning of video super-resolution with motion compensation.](https://arxiv.org/pdf/1707.00471)" German Conference on Pattern Recognition. Springer, Cham, 2017.

Makansi O, Ilg E, Brox T. End-to-end learning of video super-resolution with motion compensation[C]//German Conference on Pattern Recognition. Springer, Cham, 2017: 203-214.

#### 3.2.3 Real-time video super-resolution with spatio-temporal networks and motion compensation

Caballero, Jose, et al. "[Real-Time Video Super-Resolution with Spatio-Temporal Networks and Motion Compensation.](http://openaccess.thecvf.com/content_cvpr_2017/papers/Caballero_Real-Time_Video_Super-Resolution_CVPR_2017_paper.pdf)" CVPR. Vol. 1. No. 2. 2017.

Caballero J, Ledig C, Aitken A P, et al. Real-Time Video Super-Resolution with Spatio-Temporal Networks and Motion Compensation[C]//CVPR. 2017, 1(2): 7.

#### 3.2.4 Detail-Revealing Deep Video Super-Resolution

Tao, Xin, et al. "[Detail-revealing deep video super-resolution.](http://openaccess.thecvf.com/content_ICCV_2017/papers/Tao_Detail-Revealing_Deep_Video_ICCV_2017_paper.pdf)" Proceedings of the IEEE International Conference on Computer Vision, Venice, Italy. 2017.

Tao X, Gao H, Liao R, et al. Detail-revealing deep video super-resolution[C]//Proceedings of the IEEE International Conference on Computer Vision, Venice, Italy. 2017: 22-29.

#### 3.2.5 Deep Video Super-Resolution Network Using Dynamic Upsampling Filters Without Explicit Motion Compensation

Jo, Younghyun, et al. "[Deep Video Super-Resolution Network Using Dynamic Upsampling Filters Without Explicit Motion Compensation.](http://openaccess.thecvf.com/content_cvpr_2018/papers/Jo_Deep_Video_Super-Resolution_CVPR_2018_paper.pdf)" Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition. 2018.

Jo Y, Oh S W, Kang J, et al. Deep Video Super-Resolution Network Using Dynamic Upsampling Filters Without Explicit Motion Compensation[C]//Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition. 2018: 3224-3232.